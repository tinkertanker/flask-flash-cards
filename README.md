# flask-flash-cards

A Flask flash cards demo. Made for teaching the 2020 GCE A-level H2 Computing syllabus.

## Setup instructions
* Install Python3.
* Run `pip3 install -r requirements.txt` to install the project dependencies.
* Run the `run.sh` script to start the server.
